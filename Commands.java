package calculator;

public enum Commands {
	C("C"),
	EQUALS("="),
	NR_0("0",0),
	NR_1("1",1),
	NR_2("2",2),
	NR_3("3",3),
	NR_4("4",4),
	NR_5("5",5),
	NR_6("6",6),
	NR_7("7",7),
	NR_8("8",8),
	NR_9("9",9),
	DIV("/"),
	MUL("*"),
	MINUS("-"),
	PLUS("+");

	
	private int number;
	private String displayText;
	
	int number(){
		return number;
	}
	String displayText(){
		return displayText;
	}
	
	Commands(String displayText) {
		this(displayText, -1);
	}
	
	Commands(String displayText, int number) {
		this.displayText=displayText;
		this.number=number;
		
	}
}

