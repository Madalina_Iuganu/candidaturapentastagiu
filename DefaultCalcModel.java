package calculator;

public class DefaultCalcModel implements CalcModel {

	private double currentRezult;
	private double currentValue;
	private double lastVal;

	public DefaultCalcModel() {
		currentRezult = 0;
		currentValue = 0;
	}

	public void processCommand(Commands cmd) {
		if (cmd.number() != -1) {
			addCifre(cmd.number());
		} else {
			switch (cmd) {
			case C:
				currentValue = 0;
				lastVal = currentValue;
				currentRezult = 0;
				break;

			case MINUS:
				currentRezult = currentValue - currentRezult;
				System.out.println("CurrentRezult "+  currentRezult);
				System.out.println("CurrentValue" + currentValue);
				lastVal = currentRezult;
				currentValue = 0;
				break;

			case PLUS:
				currentRezult += currentValue;
				currentValue = 0;
				lastVal = currentRezult;
				System.out.println("lastVal" + lastVal);
				break;
				
			case MUL:
				currentRezult *= currentValue;
				currentValue = 0;
				lastVal = currentRezult;
				System.out.println("lastVal" + lastVal);
				break;
			
			case DIV:
				currentRezult /= currentValue;
				currentValue = 0;
				lastVal = currentRezult;
				System.out.println("lastVal" + lastVal);
				break;
			case EQUALS:
				switch(cmd){
				case PLUS:
					System.out.println("currentRez:"+ currentRezult);
					currentRezult+=currentValue;
				
					System.out.println("lastVal__: " + lastVal);
					lastVal=currentRezult;
				}
				
				System.out.println("lastValEQ" + lastVal);
				break;
			}
		}
	}

	public String getDisplayText() {
	
		return lastVal + "";
	}

	private void addCifre(int cifre) {
		currentValue = currentValue * 10 + cifre;
		lastVal = currentValue;
	}

}
