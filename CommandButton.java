package calculator;

import javax.swing.JButton;

public class CommandButton extends JButton{
	
	private static final long serialVersionUID = 1L;
	private Commands cmd;
	
	public Commands getCommand(){
		return cmd;
	}
	
	public CommandButton(Commands cmd){
		super(cmd.displayText());
		this.cmd=cmd;
	}

}
