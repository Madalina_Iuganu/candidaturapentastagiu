package calculator;

public interface CalcModel {
	void processCommand(Commands cmd);
	String getDisplayText();

}

