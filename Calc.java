package calculator;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calc extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JTextField txtDisplay;
	private CalcModel model;

	public Calc() {
		super("Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		model=new DefaultCalcModel();
		
		initComponents();
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	
	public void actionPerformed(ActionEvent e) {
	CommandButton btn=(CommandButton)e.getSource();
	model.processCommand(btn.getCommand());
	txtDisplay.setText(model.getDisplayText());

	}

	private void initComponents() {
		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		txtDisplay = new JTextField();
		txtDisplay.setEditable(false);
		txtDisplay.setBackground(Color.WHITE);
		txtDisplay.setHorizontalAlignment(JTextField.RIGHT);
		add(txtDisplay, BorderLayout.NORTH);

		Commands[] cmdButtons = Commands.values();

		JPanel pnlGroup1 = new JPanel();
		for (int btnIndex = 0; btnIndex < 3; btnIndex++) {
			CommandButton btn = new CommandButton(cmdButtons[btnIndex]);
			btn.addActionListener(this);
			pnlGroup1.add(btn);
		}
		add(pnlGroup1, BorderLayout.EAST);

		JPanel pnlGroup2 = new JPanel(new GridLayout(4, 5, 5, 5));
		for (int btnIndex = 3; btnIndex < cmdButtons.length; btnIndex++) {
			CommandButton btn = new CommandButton(cmdButtons[btnIndex]);
			btn.addActionListener(this);
			pnlGroup2.add(btn);
		}
		add(pnlGroup2, BorderLayout.SOUTH);
	}

}

